<?php

use Illuminate\Http\Request;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/persons', function (Request $quest) {

  $filename_data = 'people_field.txt';

  if (Storage::exists($filename_data)) {
    /*
     * If the file exist
     * Get the contents of the file
     */
    $contents = Storage::get($filename_data);

  } else {
    /*
     * If the file does not exist
     * Create a new file with an empty json
     */
    Storage::put($filename_data, $contents = json_encode([]));
  }

  // JSON decode it {$contents}
  $contents = json_decode($contents, true);

  return response()->json(['data' => $contents], 200);

});

$router->post('/persons', function (Request $request) {

    $first_names = $request->input('people.*.firstname');
    $sur_names = $request->input('people.*.surname');

    $input_fields = $request->all();

    // Guarantee some data is being received
    if (empty($first_names) || empty($sur_names)) {
      return response()->json(['error' => 'Missing required fields.'], 400);
    }

    // Guarantee received fields are arrays
    if (!is_array($first_names) || !is_array($sur_names)) {
      return response()->json(['error' => 'Invalid required fields.'], 400);
    }

    // Guarantee that the total of fields are equal
    if (count($first_names) !== count($sur_names)) {
      return response()->json(['error' => 'Total of required fields mismatch.'], 400);
    }

    // Guarantee that required fields are at least string or null
    $validator = Validator::make($input_fields, [
        'people.*.firstname' => 'nullable|string',
        'people.*.surname' => 'nullable|string',
    ]);

    if ($validator->fails()) {
        return response()->json(['error' => 'Field inputs are not valid.'], 400);
    }

    $json_fields = json_encode($input_fields);
    $filename_data = 'people_field.txt';
    
    if (!Storage::exists($filename_data)) {
      return response()->json(['error' => 'The specified storage path is not valid.'], 500);
    }

    Storage::put($filename_data, $json_fields);

    return response()->json(['message' => 'Your data has been saved succesfully.', 'data' => $input_fields], 200);

});
