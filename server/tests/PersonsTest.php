<?php

use Illuminate\Support\Facades\Storage;

class PersonsTest extends TestCase
{
    /**
     * Test if sent data is returned equally in "data" param in a successful request.
     *
     * @return void
     */
    public function testDataIsReturnedSuccessfullyInHttpResponse()
    {
        $insertion_data = [
          'people' => [
            ['firstname' => 'John', 'surname' => 'Doe'],
            ['firstname' => 'João', 'surname' => 'Rodrigues'],
          ]
        ];

        $this->json('POST', '/persons', $insertion_data)
        ->seeJsonEquals([
          'data' => $insertion_data,
          'message' => 'Your data has been saved succesfully.'
        ]);
    }

    /**
     * Test if a 200 status code is received upon a successful update.
     *
     * @return void
     */
    public function testSuccessHttpStatusCodeIsReturnedUponSuccessfulUpdate()
    {
        $insertion_data = [
          'people' => [
            ['firstname' => 'John', 'surname' => 'Doe'],
            ['firstname' => 'João', 'surname' => 'Rodrigues'],
          ]
        ];

        $response = $this->call('POST', '/persons', $insertion_data);

        $this->assertEquals(200, $response->status());
    }

    /**
     * Test if a 400 status code is received if no data is sent.
     *
     * @return void
     */
    public function testBadRequestHttpStatusCodeIsReceivedIfNoDataIsSent()
    {
        $insertion_data = [];

        $response = $this->call('POST', '/persons', $insertion_data);

        $this->assertEquals(400, $response->status());
    }

    /**
     * Test if a 400 status code is received when non array values are sent.
     *
     * @return void
     */
    public function testBadRequestHttpStatusCodeIsReceivedIfNotArrayDataIsSent()
    {
        $insertion_data = [null];

        $response = $this->call('POST', '/persons', $insertion_data);

        $this->assertEquals(400, $response->status());
    }

    /**
     * Test if a 500 status code is received when the file does not exist.
     *
     * @return void
     */
    public function testInternalServerErrorHttpStatusCodeIsReceivedIfDataFileDoesNotExist()
    {
        $filename = 'people_field.txt';

        // Store backup of file contents
        if (Storage::exists($filename)) {
          $backup_contents = Storage::get($filename);
        } else {
          $backup_contents = json_encode([], JSON_FORCE_OBJECT);
        }

        // Delete file
        Storage::delete($filename);

        $insertion_data = [
          'people' => [
            ['firstname' => 'John', 'surname' => 'Doe'],
            ['firstname' => 'João', 'surname' => 'Rodrigues'],
          ]
        ];

        $response = $this->call('POST', '/persons', $insertion_data);

        $this->assertEquals(500, $response->status());

        // Create file again after test
        Storage::put($filename, $backup_contents);
    }

    /**
     * Test if expected output is succesully returned from storage.
     * For that we set the data with a set array and check if the contents
     * of the file matches what we send in the POST request.
     *
     * @return void
     */
    public function testDataIsSuccessfullyReturned()
    {
        $insertion_data = [
          'people' => [
            ['firstname' => 'John', 'surname' => 'Doe'],
            ['firstname' => 'João', 'surname' => 'Rodrigues'],
          ]
        ];

        $this->call('POST', '/persons', $insertion_data);

        $this->json('GET', '/persons')
        ->seeJsonEquals([
          'data' => $insertion_data
        ]);;
    }

    /**
     * Test if a 400 status code is received when wrong data types are sent.
     *
     * @return void
     */
    public function testBadRequestHttpStatusCodeIsReceivedWhenWrongDataTypesAreSent()
    {
        $insertion_data = [
          'people' => [
            ['firstname' => 'John', 'surname' => 'Doe'],
            ['firstname' => 'João', 'surname' => 2],
          ]
        ];

        $response = $this->call('POST', '/persons', $insertion_data);

        $this->assertEquals(400, $response->status());
    }
}
