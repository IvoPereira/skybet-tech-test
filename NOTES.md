# Sky Betting & Gaming Technical Test

## Technology Stack

The technology stack chosen for this project was based on:

* **Lumen** - A PHP micro-framework based on Laravel for the server-side.
Lumen is a framework that I've wanted to give it a go for a while ago. It is used mainly for small projects that consist on REST APIs and client-side consumers like React or Vue.js, so I've seen it as the perfect candidate for this project by letting me learn it while using it in a real-world project.

* **Vue.js** - A client-side technology I've been using for some small projects in the past too. I am used to working with React.js too, however I find much less time consuming to get a project started in Vue than in React.

## The project

The project is distributed in two distinct folders:

* **/server**
Has everything related to the Lumen application and provides all the endpoints for the client-side.

* **/client**
Home for the Vue.js installation and provides the client interface when the final user should access.

The architecture setup on the server and client were created based on assumptions of what could be a general validation for the specific feature.

## First-time setup

As a first-time user, you need to download packages required for *server* and *client*.

You can download packages for the **server** by executing the command  `composer install`.

Download the missing packages for **client** by executing the command `npm install`.

## Quick-start

*Take in attention that every command referred here, is based on being executed on the base path of the project.*


----------


To run the project the PHP server should be started first, so the client-side could be able to communicate with it when needed.

You can run the **server** by typing `cd server; php -S localhost:8000 -t public`

Once server is online, you are able to access it in *localhost:8000*.

You can run the **client** by typing `cd client; npm run dev` in another terminal instance.

You can confirm persons information retrieval and submission is working as intended by inspecting network and verifying information being sent and retrieved.

## Data store

All the information is stored in a file located in *server/storage/app/people_field.txt.*

## Tests

You can find written tests for **server** in *server/tests*.

**Execute tests** by running `cd server; ./vendor/bin/phpunit`
